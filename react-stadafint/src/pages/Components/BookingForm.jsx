import React from 'react';

const BookingForm = ({ handleNewBooking, newBooking, setNewBooking}) => {

   return (
      <div className='book-cleaning-container'>
         <h2 className='book-cleaning-title'>Boka städning</h2>
         <form className='book-form' onSubmit={handleNewBooking}>
            <div className='cleaner-container'>
               <label >Välj städare</label>
               <select className='btn-css' name="" id="" onChange={(e) => setNewBooking({ ...newBooking, cleaner: e.target.value })} value={newBooking.cleaner}>
                  <option value="" disabled hidden >Städare</option>
                  <option value="Olle">Olle</option>
                  <option value="Mia">Mia</option>
                  <option value="Pär">Pär</option>
                  <option value="Nina">Nina</option>
               </select>
            </div>
            <div className='date-container'>
               <label >Välj datum</label>
               <input type="date" value={newBooking.date} onChange={(e) => setNewBooking({ ...newBooking, date: e.target.value })} name="" id="" />
               <label >Välj tid</label>
               <input type="time" value={newBooking.time} onChange={(e) => setNewBooking({ ...newBooking, time: e.target.value })} name="" id="" />
            </div>
            <div className='service-container'>
               <label htmlFor="basic">
                  <input type="radio" checked={"Basic" === newBooking.service} name='service' value="Basic" onChange={(e) => setNewBooking({ ...newBooking, service: e.target.value })} />
                  Basic</label>

               <label htmlFor='topp' >
                  <input type="radio" checked={"Topp" === newBooking.service}  name='service' value='Topp' onChange={(e) => setNewBooking({ ...newBooking, service: e.target.value })} />
                  Topp</label>

               <label htmlFor='diamant' >
                  <input type="radio" checked={"Diamant" === newBooking.service}  name='service' value='Diamant' onChange={(e) => setNewBooking({ ...newBooking, service: e.target.value })} />
                  Diamant</label>

               <label htmlFor='fönstertvätt' >
                  <input type="radio" checked={"Fönstertvätt" === newBooking.service}  name='service' value='Fönstertvätt' onChange={(e) => setNewBooking({ ...newBooking, service: e.target.value })} />
                  Fönstertvätt</label>
            </div>
            <button className='book-now-btn btn-css' type='submit'  >Boka nu</button>
         </form>
      </div>
   )
}

export default BookingForm

