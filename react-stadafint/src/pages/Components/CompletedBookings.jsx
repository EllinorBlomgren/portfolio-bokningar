import React from 'react'

const CompletedBookings = ({bookings, handleCheckboxChange, handleDeleteCheckedItems, checkedItems}) => {
   return (
      <div className='done-cleanings-container'>
         <h2>Utförda bokningar</h2>
         <ul className='booking-ul'>
            {bookings.filter(booking => booking.completed)
               .sort((a, b) => new Date(a.date) - new Date(b.date))
               .map((booking, index) => (

                  <li className='booking-list' key={index}>
                     <p>{booking.cleaner}</p>
                     <p>{booking.date}</p>
                     <p>{booking.time}</p>
                     <p>{booking.service}</p>
                     <input type='checkbox' checked={checkedItems.includes(booking.id)} onChange={() => handleCheckboxChange(booking.id)} />
                  </li>
               ))}
         </ul>
         <button className='delete-selected-btn btn-css' onClick={handleDeleteCheckedItems}>Radera</button>
      </div>
   )
}

export default CompletedBookings
