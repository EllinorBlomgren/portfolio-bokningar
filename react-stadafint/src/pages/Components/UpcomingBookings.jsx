import React from 'react'

const UpcomingBookings = ({ bookings, handleDone, handleDeleteItems }) => {
  return (
    <div className='upcoming-bookings-container'>
      <h2>Kommande bokningar</h2>
      <ul className='booking-ul'>
        {bookings.filter(booking => !booking.completed)
          .sort((a, b) => new Date(a.date) - new Date(b.date))
          .map((booking, index) => (

            <li className='booking-list' key={index}>
              <p>{booking.cleaner}</p>
              <p>{booking.date}</p>
              <p>{booking.time}</p>
              <p>{booking.service}</p>
              <button onClick={() => handleDone(booking.id)}>👍</button>
              <button className='btn-trash' onClick={() => handleDeleteItems(booking.id)}>🗑️</button>
            </li>
          ))}
      </ul>
    </div>
  )
}

export default UpcomingBookings