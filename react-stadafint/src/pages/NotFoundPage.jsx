import React from 'react';
import { Link } from 'react-router-dom';

const NotFoundPage = () => {
    return(
        <div>
            <h1>Page Not Found</h1>
            <p>The requested page could not be found.</p>
            <Link to="/">Go back</Link>
        </div>
    );
};

export default NotFoundPage;