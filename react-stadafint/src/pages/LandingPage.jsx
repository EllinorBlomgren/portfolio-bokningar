import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import "./Components/LandingPage.css"
import logo from "../assets/vacuum-cleaner-icon.png"
import account from "../assets/account-icon.webp"
import gloves from "../assets/cleangloves.jpg"
import accept from "../assets/accept-icon.webp"
import Services from '../Components/Services'
import Testimonials from '../Components/Testimonials'
import Footer from '../Components/footer'



function LandingPage() {
   const navigate = useNavigate();

   const handleBooking = () => {
      navigate("/booking")
   }


   return (<>
      <div className='header'>
         <div className='navbar'>
            <div className='nav-left'>
               <img src={logo} alt="vacuum cleaner" />
               <h1>StädaFint</h1>
            </div>
            <div className='nav-center'>
               <a href="#">Tjänster</a>
               <a href="#">Om oss</a>
               <a href="#">Kontakt</a>
            </div>
            <div className='nav-right'>
               <a href="#">Företag</a>
               <div className='nav-right-pages'>
                  <button className='nav-right-btn' onClick={handleBooking}>
                     <img src={account} alt="" />
                     <p>Mina sidor</p>
               </button>
               </div>
            </div>
         </div>
         <div className='header-hero'>
            <div className='hero-formimg'>
               <div className='hero-form'>
                  <h2>Vi städar fint hemma hos dig</h2>
                  <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit</p>
                  <form action="">
                     <input type="text" placeholder='Postnummer' />
                     <select id="services" name="services" >
                        <option value="basic" disabled selected hidden>Välj tjänst</option>
                        <option value="basic">Basic</option>
                        <option value="topp">Topp</option>
                        <option value="diamant">Diamant</option>
                        <option value="fönstertvätt">Fönstertvätt</option>
                     </select>
                     <button className='herobtn' type='button' onClick>Gå vidare</button>

                  </form>
               </div>
               <div className='hero-img'>
                  <img src={gloves} alt="gloves" />
               </div>
            </div>
            <div className='hero-check'>
               <img src={accept} alt="accept" />
               <p className='check-text'>Prisgaranti</p>
               <img src={accept} alt="accept" />
               <p className='check-text'>Miljövänliga produkter</p>
               <img src={accept} alt="accept" />
               <p className='check-text'>Försäkrat</p>
            </div>
         </div>
      </div>
      <div className='main'>
         
      <Services />
      </div>
      <div className="testimonials">
         <Testimonials />
      </div>
      <div className='footer'>
         <Footer/>
      </div>
   </>)
}

export default LandingPage;