import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import axios from 'axios';
import "./Components/MyBookingPage.css"
import BookingForm from './Components/BookingForm';
import UpcomingBookings from './Components/UpcomingBookings';
import CompletedBookings from './Components/CompletedBookings';



const MyBookingPage = () => {

    const [user, setUser] = useState([])

    const [bookings, setBookings] = useState([]);
    const [newBooking, setNewBooking] = useState({
        cleaner: '',
        date: '',
        time: '',
        service: '',
        kund: 'Elidanora',
        completed: false
    });
    const [checkedItems, setCheckedItems] = useState([]);
    const navigate = useNavigate();


    //Hämta bokningar från db.json
    const fetchData = async () => {

        try {
            const response = await axios.get('http://localhost:3000/booking');
            const fetchedBookings = response.data;

            setBookings(fetchedBookings);

        } catch (error) {
            console.error('Error:', error);
        }
    };

    useEffect(() => {
        fetchData();
    }, []);

    //Användarnamn
    useEffect(() => {
        const fetchCustomer = async () => {
            try {
                const response = await axios.get('http://localhost:3000/booking');
                if (response.data.length > 0) {                             // kollar om bokningar inte är tom
                    setUser(response.data[0].kund);
                }
                // setUser(customerData);
            } catch (error) {
                console.error('Error', error)
            }
        };
        fetchCustomer();
    }, []);


    // Skapa ny bokning
    const handleNewBooking = async (e) => {
        e.preventDefault()

        // Kollar om något av fälten är tomma        
        if (!newBooking.cleaner || !newBooking.date || !newBooking.time || !newBooking.service) {
            alert('Vänligen fyll i alla rutor.');
            return;
        }
        try {


            // Kontrollerar om det finns en bokning med samma datum och tid - för att undvika dubbelbokning           
            const isExistingBooking = bookings.some(booking =>
                booking.cleaner === newBooking.cleaner && booking.date === newBooking.date && booking.time === newBooking.time
            );
            if (isExistingBooking) {
                alert('Det finns redan en bokning för detta datum och tid. Välj en annan tid.');
                return;
            };

            //post för att lägga till den nya bokningen
            const response = await axios.post('http://localhost:3000/booking', newBooking);
            const fetchedBookings= response.data;

            //Uppdaterar listan med nya bokningen
            setBookings(prevBookings => [...prevBookings, response.data]);
            setNewBooking({
                cleaner: '',
                date: '',
                time: '',
                service: '',
                kund: "Elidanora",
                completed: false
            })
        } catch (error) {
            console.error('Error adding booking:', error);
        }
    }

    const handleCheckboxChange = (id) => {
        const updatedCheckedItems = checkedItems.includes(id)
            ? checkedItems.filter(item => item !== id)
            : [...checkedItems, id];
        setCheckedItems(updatedCheckedItems);
    };

    const handleDeleteCheckedItems = () => {
        try {
            checkedItems.forEach(async id => {
                await axios.delete(`http://localhost:3000/booking/${id}`);
            });

            const fetchedBookings = bookings.filter(booking => !checkedItems.includes(booking.id));
            setBookings(fetchedBookings);
            setCheckedItems([]);

        } catch (error) {
            console.error('Error deleting booking:', error)
        }
    }

    const handleDeleteItems = async (id) => {
        try {
            await axios.delete(`http://localhost:3000/booking/${id}`);
            setBookings(prevBookings => prevBookings.filter(booking => booking.id !== id));
        } catch (error) {
            console.error('Error deleting booking:', error);
        }
    };

    const handleDone = async (id) => {
        try {
            await axios.patch(`http://localhost:3000/booking/${id}`, { completed: true });
            setBookings(prevBookings => {
                return prevBookings.map(booking => {
                    if (booking.id === id) {
                        return { ...booking, completed: true };
                    }
                    return booking;
                });
            });
        } catch (error) {
            console.error('Error marking booking as completed:', error);
        }
    };

    return (
        <div className='booking-container'>
            <div>
                <h1 className='booking-title'>Välkommen, {user}!</h1>
            </div>
                <BookingForm handleNewBooking={handleNewBooking} newBooking={newBooking} setNewBooking={setNewBooking} />
                <UpcomingBookings bookings={bookings} handleDone={handleDone} handleDeleteItems={handleDeleteItems}/>
                <CompletedBookings bookings={bookings} handleCheckboxChange={handleCheckboxChange} handleDeleteCheckedItems={handleDeleteCheckedItems} checkedItems={checkedItems}/>
            <button className='goback-btn btn-css' onClick={() => navigate("/")}>Tillbaka</button>
        </div>

        
    );
};

export default MyBookingPage;