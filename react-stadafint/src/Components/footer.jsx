import React from 'react';
import './footer.css';
import logo from '../assets/vacuum-cleaner-icon.png'
import facebook from '../assets/iconfacebbok.png'
import insta from '../assets/black-instagram-icon.webp'

function footer() {


    return(
        <>
            <div className='footer-container'>
                <div className='column'>
                    <div className='img-text'>
                        <img src={logo} alt="vacuum cleaner" />
                        <h1>StädaFint</h1>
                        </div>
                        <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Omnis sequi magni i laboriosam ad qearat dolores nobis dolorem.</p>
                        <div className='social-icons'>
                            <img src={facebook} alt="Facebook icon" />
                            <img src={insta} alt="Instagram icon" />
                        </div>
                    </div>
                <div className='column'>
                    <h3>Meny</h3>
                    <ul>
                        <li><a href="#">Basicstädning</a></li>
                        <li><a href="#">Toppstädning</a></li>
                        <li><a href="#">Diamantstädning</a></li>
                        <li><a href="#">Om oss</a></li>
                        <li><a href="#">Kontakt</a></li>
                    </ul>
                </div>
                <div className='column'>
                <h3>Meny</h3>
                    <ul>
                        <li><a href="#">Basicstädning</a></li>
                        <li><a href="#">Toppstädning</a></li>
                        <li><a href="#">Diamantstädning</a></li>
                        <li><a href="#">Om oss</a></li>
                        <li><a href="#">Kontakt</a></li>
                    </ul>
                </div>
                <div className='column'>
                <h3>Meny</h3>
                    <ul>
                        <li><a href="#">Basicstädning</a></li>
                        <li><a href="#">Toppstädning</a></li>
                        <li><a href="#">Diamantstädning</a></li>
                        <li><a href="#">Om oss</a></li>
                        <li><a href="#">Kontakt</a></li>
                    </ul>
                </div>
            </div>
        </>
    )
}

export default footer
