import React from 'react'
import './services.css'


const Services = () => {
  return (
    <>
        <div className="header-text">
            <h1>Våra Tjänster</h1>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. At, minima! Totam iure nostrum nulla quos deleniti dolores ex odio dolorum!</p>
        </div>
        <div className="container">
            <div className="service-card">
                <img src="src\assets\clean1.jpg" alt="Basic-clean-image" />
                <h1><b>Basic</b></h1>
                <p className='service-txt'>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
                <button className='service-btn'>Läs Mer </button>
            </div>
            <div className="service-card">
                <img src="src\assets\clean2.jpg" alt="Topp-clean-image" />
                <h1><b>Topp</b></h1>
                <p className='service-txt'>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
                <button className='service-btn'>Läs Mer </button>
            </div>
            <div className="service-card">
                <img src="src\assets\clean3.jpg" alt="Diamond-clean-image" />
                <h1><b>Diamant</b></h1>
                <p className='service-txt'>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
                <button className='service-btn'>Läs Mer </button>
            </div>
            <div className="service-card">
                <img src="src\assets\clean4.jpg" alt="Window-clean-image" />
                <h1><b>Fönstertvätt</b></h1>
                <p className='service-txt'>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
                <button className='service-btn'>Läs Mer </button>
            </div>
        </div>
        <div className="why-us-container">
            <div className="why-img">
                <img src="src\assets\clean.jpg" alt="" />
            </div>
            <div className="why-text">
                <h1>Varför Välja oss?</h1>
                <p>&#10004; <b>Vi erbjuder prisgaranti</b></p>
                <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit.</p>
                <p>&#10004; <b>Alla våra produkter är miljövänliga</b></p>
                <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit.</p>
                <p>&#10004; <b>Vi är försäkrade</b></p>
                <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit.</p>
            </div>
        </div>
    </>
  )
}

export default Services
