import React, { useState, useEffect } from 'react';
import './testimonials.css';

const Services = () => {
    const [testimonials] = useState([
        { name: "John", image: "src/Components/Images/testimonialImg1.jpg" },
        { name: "Emma", image: "src/Components/Images/testimonialImg2.jpg" },
        { name: "Anton", image: "src/Components/Images/testimonialImg3.jpg" },
        { name: "Sofia", image: "src/Components/Images/testimonialImg4.jpg" },
        { name: "Noah", image: "src/Components/Images/testimonialImg1.jpg" },
        { name: "Olivia", image: "src/Components/Images/testimonialImg2.jpg" },
        { name: "William", image: "src/Components/Images/testimonialImg3.jpg" },
        { name: "Hannah", image: "src/Components/Images/testimonialImg4.jpg" },
        { name: "Robert", image: "src/Components/Images/testimonialImg1.jpg" },
        { name: "Isabella", image: "src/Components/Images/testimonialImg2.jpg" }
    ]);

    const [testimonialsData] = useState([
        "Fantastiskt jobb! Mitt hem känns som en frisk vind.",
        "Professionellt bemötande. De lämnar inget åt slumpen, helt otroligt.",
        "Snabb och effektiv service. Renlighet och ordning på hög nivå.",
        "Imponerande noggrannhet. Varje yta skriker av skinande renhet.",
        "Pålitliga och vänliga personal. Mitt hem är som nytt igen.",
        "Överlägsen kvalitet! Jag är så nöjd med resultatet.",
        "Miljövänlig rengöring. En fräsch doft utan skadliga kemikalier.",
        "Otrolig noggrannhet. Jag är mycket imponerad av deras arbete.",
        "Högsta standard! De levererar alltid utmärkta resultat, gång på gång.",
        "Enastående service. Jag rekommenderar dem starkt till alla vänner och familj."
    ]);

    const rating = [
        "⭐⭐⭐⭐⭐",
        "⭐⭐⭐⭐",
    ];

    const [usedIndexes, setUsedIndexes] = useState([]);

    const getRandomTestimonial = () => {
        const randomIndex = Math.floor(Math.random() * testimonialsData.length);
        return testimonialsData[randomIndex];
    };

    const getRandomRating = () => {
        const randomIndex = Math.floor(Math.random() * rating.length);
        return rating[randomIndex];
    };

    useEffect(() => {
        const shuffledIndexes = [...Array(testimonials.length).keys()].sort(() => Math.random() - 0.5);
        const newUsedIndexes = shuffledIndexes.slice(0, 4);
        setUsedIndexes(newUsedIndexes);
    }, [testimonials.length]);

    const testimonialCards = usedIndexes.map((index, i) => {
        const testimonial = testimonials[index];
        return (
            <div key={i} className="testimonial-card">
                <img src={testimonial.image} alt="testimonial" />
                <h1><b>{testimonial.name}</b></h1>
                <p>{getRandomRating()}</p>
                <p className='testimonial-txt'>{getRandomTestimonial()}</p>
            </div>
        );
    });

    return (
        <>
            <div className="header-text">
                <h1>Vad säger våra kunder?</h1>
            </div>
            <div className="container">
                {testimonialCards}
            </div>
        </>
    );
};

export default Services;
