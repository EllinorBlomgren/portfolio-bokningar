import { useState } from 'react'
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import './App.css'
import LandingPage from './pages/LandingPage';
import MyBookingPage from './pages/MyBookingPage';
import NotFoundPage from './pages/NotFoundPage'


function App() {

  return (
    <BrowserRouter>
      <>
        <Routes>
          <Route exact path="/" element={<LandingPage/>}/>
          <Route exact path="/booking" element={<MyBookingPage/>}/>
          <Route path="*" element={<NotFoundPage/>}/>
        </Routes>
      </>
    </BrowserRouter>
   
  )
}

export default App
